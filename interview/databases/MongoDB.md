
# Основные концепции и операции в MongoDB

MongoDB — это популярная документно-ориентированная база данных NoSQL, которая отличается гибкостью, масштабируемостью и высокой производительностью. Вот основные концепции и операции в MongoDB:

## Основные Концепции

### Документы
- Основная единица данных в MongoDB — это документ, который представляет собой структуру данных в формате BSON (Binary JSON). Документ может содержать вложенные массивы и другие документы.
- Пример документа:
  ```json
  {
    "_id": ObjectId("507f1f77bcf86cd799439011"),
    "name": "John Doe",
    "age": 29,
    "address": {
      "street": "123 Main St",
      "city": "Anytown"
    }
  }
  ```

### Коллекции
- Документы хранятся в коллекциях, которые аналогичны таблицам в реляционных базах данных. Коллекция содержит набор документов и не имеет схемы, что позволяет хранить документы с различными структурами в одной коллекции.

### Базы данных
- В MongoDB базы данных содержат коллекции. Одна инстанция MongoDB может иметь несколько баз данных.

### _id Поле
- Каждый документ имеет уникальное поле `_id`, которое автоматически генерируется MongoDB, если его не указать. `_id` можно использовать как первичный ключ.

## Основные Операции в MongoDB (Java)

### Подключение к MongoDB

Сначала необходимо подключиться к MongoDB и получить доступ к нужной базе данных и коллекции:

```java
import com.mongodb.MongoClient;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import org.bson.Document;

public class MongoDBExample {
    public static void main(String[] args) {
        // Подключение к MongoDB
        MongoClient mongoClient = new MongoClient("localhost", 27017);
        MongoDatabase database = mongoClient.getDatabase("your_database_name");
        MongoCollection<Document> collection = database.getCollection("your_collection_name");
    }
}
```

### Создание (Insert)

```java
// Вставка одного документа
Document document1 = new Document("name", "Alice")
                        .append("age", 30);
collection.insertOne(document1);

// Вставка нескольких документов
List<Document> documents = Arrays.asList(
    new Document("name", "Bob").append("age", 25),
    new Document("name", "Charlie").append("age", 35)
);
collection.insertMany(documents);
```

### Чтение (Query)

```java
// Поиск документов по имени
FindIterable<Document> docsByName = collection.find(new Document("name", "Alice"));
for (Document doc : docsByName) {
    System.out.println(doc.toJson());
}

// Поиск документов с возрастом больше 25
FindIterable<Document> docsByAge = collection.find(new Document("age", new Document("$gt", 25)));
for (Document doc : docsByAge) {
    System.out.println(doc.toJson());
}
```

### Обновление (Update)

```java
// Обновление одного документа
collection.updateOne(new Document("name", "Alice"), new Document("$set", new Document("age", 31)));

// Обновление нескольких документов
collection.updateMany(new Document("age", new Document("$lt", 30)), new Document("$set", new Document("status", "young")));
```

### Удаление (Delete)

```java
// Удаление одного документа
collection.deleteOne(new Document("name", "Alice"));

// Удаление нескольких документов
collection.deleteMany(new Document("age", new Document("$gt", 30)));
```

### Агрегация (Aggregation)

```java
// Пример агрегации
List<Bson> pipeline = Arrays.asList(
    Aggregates.match(new Document("status", "active")),
    Aggregates.group("$age", Accumulators.sum("total", 1))
);

AggregateIterable<Document> result = collection.aggregate(pipeline);
for (Document doc : result) {
    System.out.println(doc.toJson());
}
```

Эти примеры показывают, как выполнять основные операции с MongoDB на языке программирования Java. 
Обратите внимание, что для работы с MongoDB необходимо добавить в проект соответствующие зависимости, например, используя Maven:

```xml
<dependency>
    <groupId>org.mongodb</groupId>
    <artifactId>mongodb-driver-sync</artifactId>
    <version>4.3.3</version>
</dependency>
```

Эти зависимости можно добавить в файл `pom.xml` вашего проекта.

## Репликация и Шардинг

### Репликация
- Процесс копирования данных между несколькими серверами для обеспечения отказоустойчивости и доступности данных. MongoDB использует репликационные наборы (replica sets), где один сервер является основным (primary), а остальные — вторичными (secondary).

### Шардинг
- Метод горизонтального разделения данных на части (shards) для увеличения производительности и масштабируемости. Каждая часть представляет собой независимый набор данных, который может быть размещен на отдельном сервере.

## Индексы

```java
// Создание индекса на поле "name"
collection.createIndex(new Document("name", 1));

// Создание уникального индекса на поле "email"
collection.createIndex(new Document("email", 1), new IndexOptions().unique(true));
```

Эти концепции и операции являются основой для работы с MongoDB, позволяя гибко и эффективно управлять данными.

# Ресурсы для изучения MongoDB на русском языке

Для более углубленного изучения MongoDB на русском языке, есть несколько бесплатных ресурсов, которые могут быть полезны:

## Официальная документация MongoDB
Официальный сайт MongoDB предлагает подробную документацию, переведенную на русский язык. Здесь можно найти руководства, примеры и справочные материалы.
- [Документация MongoDB на русском](https://docs.mongodb.com/manual/?locale=ru)

## MongoDB University
MongoDB University предлагает бесплатные онлайн-курсы, которые охватывают различные аспекты работы с MongoDB. Некоторые курсы имеют субтитры на русском языке.
- [MongoDB University](https://university.mongodb.com/)

## Книга "MongoDB: руководство по эксплуатации"
Это бесплатная книга на русском языке, которая детально описывает основы и продвинутые техники работы с MongoDB.
- [Скачать книгу](https://rutracker.org/forum/viewtopic.php?t=4864481)

## YouTube-каналы
На YouTube есть множество видеоуроков на русском языке, посвященных MongoDB. Ведущие показывают на практике, как работать с этой СУБД.
- Пример канала: [ITDoctor](https://www.youtube.com/channel/UCwHL6WHUarjGfUM_52pA4yQ) (видео по MongoDB можно найти через поиск по каналу).

## Онлайн-курсы на русском языке
Платформы, такие как Coursera и Udemy, предлагают курсы по MongoDB с русскими субтитрами или полностью на русском языке. Часто эти курсы бывают бесплатными или имеют пробный период.
- Пример курса: [Coursera](https://www.coursera.org/courses?query=mongodb&lang=ru)

## Форумы и сообщества
Участие в форумах и сообществах, таких как Stack Overflow и специализированные группы в соцсетях, может помочь в решении конкретных вопросов и обмене опытом.
- Пример: [Stack Overflow на русском](https://ru.stackoverflow.com/questions/tagged/mongodb)

Эти ресурсы помогут вам получить глубокие знания и навыки работы с MongoDB, начиная от основ и до продвинутых техник.
